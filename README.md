# extensions.gnome.org frontend

This is work-in-progress rewrite of extensions.gnome.org UI. Look [to the e.g.o.'s issue](https://gitlab.gnome.org/Infrastructure/extensions-web/-/issues/116) for details.

## Develop inside container

It's possible to use [VS Code](https://code.visualstudio.com/) Remote Containers
([devcontainer](https://containers.dev/)) for fast development workspace setup.

Please look to the [nE0sIghT/ego-devcontainer](https://gitlab.gnome.org/nE0sIghT/ego-devcontainer)
repository for instructions.
